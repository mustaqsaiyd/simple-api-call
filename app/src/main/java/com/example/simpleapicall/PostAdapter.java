package com.example.simpleapicall;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostHolder> {

    Context context;

    private List<Post> PostDataList;

    public PostAdapter(Context context, List<Post> postDataList) {
        this.context = context;
        PostDataList = postDataList;
    }

    @NonNull
    @Override
    public PostHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.row_items, parent, false);
        return new PostHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostHolder postHolder, int i) {

        postHolder.tvId.setText(PostDataList.get(i).getId().toString());
        postHolder.tvAlbumid.setText(PostDataList.get(i).getAlbumId().toString());
        postHolder.tvTitle.setText(PostDataList.get(i).getTitle());
        postHolder.tvUrl.setText(PostDataList.get(i).getUrl());
    }

    @Override
    public int getItemCount() {
        return PostDataList.size();
    }

    public class PostHolder extends RecyclerView.ViewHolder {
        TextView tvAlbumid,tvId,tvTitle,tvUrl;

        public PostHolder(@NonNull View itemView) {
            super(itemView);
            tvAlbumid=itemView.findViewById(R.id.tvAlbumId);
            tvId=itemView.findViewById(R.id.tvId);
            tvTitle=itemView.findViewById(R.id.tvTitle);
            tvUrl=itemView.findViewById(R.id.tvUrl);

        }
    }
}
